# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <bald@smail.ee>, 2012, 2013, 2014, 2016, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-31 00:46+0000\n"
"PO-Revision-Date: 2020-04-14 15:22+0300\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.12.3\n"

#: plasmoid/package/contents/ui/PopupDialog.qml:26
msgid "Search…"
msgstr ""

#: plasmoid/package/contents/ui/PopupDialog.qml:71
#: plasmoid/package/contents/ui/printmanager.qml:52
msgid "No printers have been configured or discovered"
msgstr "Ühtegi printerit pole seadistatud või tuvastatud"

#: plasmoid/package/contents/ui/PopupDialog.qml:71
msgid "No matches"
msgstr ""

#: plasmoid/package/contents/ui/PrinterItem.qml:25
msgid "Resume printing"
msgstr "Jätka trükkimist"

#: plasmoid/package/contents/ui/PrinterItem.qml:25
msgid "Pause printing"
msgstr "Trükkimine pausile"

#: plasmoid/package/contents/ui/PrinterItem.qml:37
msgid "Configure printer..."
msgstr "Seadista printerit ..."

#: plasmoid/package/contents/ui/PrinterItem.qml:42
msgid "View print queue"
msgstr "Ava trükijärjekord"

#: plasmoid/package/contents/ui/printmanager.qml:31
msgid "Printers"
msgstr "Printerid"

#: plasmoid/package/contents/ui/printmanager.qml:36
msgid "There is one print job in the queue"
msgid_plural "There are %1 print jobs in the queue"
msgstr[0] "Järjekorras on üks trükitöö"
msgstr[1] "Järjekorras on %1 trükitööd"

#: plasmoid/package/contents/ui/printmanager.qml:45
msgctxt "Printing document name with printer name"
msgid "Printing %1 with %2"
msgstr "%1 trükkimine printeris %2"

#: plasmoid/package/contents/ui/printmanager.qml:47
msgctxt "Printing with printer name"
msgid "Printing with %1"
msgstr "Trükkimine printeris %1"

#: plasmoid/package/contents/ui/printmanager.qml:50
msgid "Print queue is empty"
msgstr "Trükijärjekord on tühi"

#: plasmoid/package/contents/ui/printmanager.qml:126
#, fuzzy
#| msgid "All jobs"
msgid "Show All Jobs"
msgstr "Kõik tööd"

#: plasmoid/package/contents/ui/printmanager.qml:132
#, fuzzy
#| msgid "Completed jobs only"
msgid "Show Only Completed Jobs"
msgstr "Ainult lõpetatud tööd"

#: plasmoid/package/contents/ui/printmanager.qml:138
#, fuzzy
#| msgid "No active jobs"
msgid "Show Only Active Jobs"
msgstr "Aktiivseid töid pole"

#: plasmoid/package/contents/ui/printmanager.qml:149
msgid "&Configure Printers..."
msgstr "&Seadista printereid..."

#~ msgid "Search for a printer..."
#~ msgstr "Printeri otsimine..."

#~ msgid "General"
#~ msgstr "Üldine"

#~ msgid "Active jobs only"
#~ msgstr "Ainult aktiivsed tööd"

#~ msgid "%1, no active jobs"
#~ msgstr "%1, aktiivseid töid ei ole"

#~ msgid "%1, %2 active job"
#~ msgid_plural "%1, %2 active jobs"
#~ msgstr[0] "%1, %2 aktiivne tööd"
#~ msgstr[1] "%1, %2 aktiivset tööd"

#~ msgid "%1, no jobs"
#~ msgstr "%1, töid ei ole"

#~ msgid "%1, %2 job"
#~ msgid_plural "%1, %2 jobs"
#~ msgstr[0] "%1, %2 töö"
#~ msgstr[1] "%1, %2 tööd"

#~ msgid "One active job"
#~ msgid_plural "%1 active jobs"
#~ msgstr[0] "Üks aktiivne töö"
#~ msgstr[1] "%1 aktiivset tööd"

#~ msgid "One job"
#~ msgid_plural "%1 jobs"
#~ msgstr[0] "Üks töö"
#~ msgstr[1] "%1 tööd"

#~ msgid "Only show jobs from the following printers:"
#~ msgstr "Ainult järgmiste printerite tööd:"

#~ msgid "Cancel"
#~ msgstr "Katkesta"

#~ msgid "Hold"
#~ msgstr "Ootele"

#~ msgid "Release"
#~ msgstr "Vabasta"

#~ msgid "Owner:"
#~ msgstr "Omanik:"

#~ msgid "Size:"
#~ msgstr "Suurus:"

#~ msgid "Created:"
#~ msgstr "Loodud:"

#~ msgid ""
#~ "There is currently no available printer matching the selected filters"
#~ msgstr "Praegu pole saadaval ühtegi valitud filtritega sobivat printerit"
